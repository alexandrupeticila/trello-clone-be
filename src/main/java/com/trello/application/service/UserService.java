package com.trello.application.service;


import com.trello.application.entity.User;
import com.trello.application.exception.UserServiceException;
import com.trello.application.model.request.LoginModelRequest;
import com.trello.application.model.request.UserModelRequest;
import com.trello.application.model.response.ErrorMessages;
import com.trello.application.model.response.LoginModelResponse;
import com.trello.application.model.response.UserRest;
import com.trello.application.repository.UserRepository;
import com.trello.application.security.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
@RequiredArgsConstructor
@Service
public class UserService {
    private static final ModelMapper MAPPER = new ModelMapper();
    private final UserRepository repository;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final PasswordEncoder passwordEncoder;
    private final SequenceGenerator sequenceGenerator;
    private final Util util;


    public LoginModelResponse authenticate(LoginModelRequest loginInfo) {
        User user = repository.findByEmail(loginInfo.getEmail());

        if (user == null) {
            log.info(ErrorMessages.NO_USER_WITH_THIS_EMAIL.getErrorMessage());
            throw new UserServiceException(ErrorMessages.NO_USER_WITH_THIS_EMAIL.getErrorMessage());
        }

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginInfo.getEmail(), loginInfo.getPassword()));
            LoginModelResponse response = new LoginModelResponse();
            String jwt = jwtTokenProvider.createToken(loginInfo.getEmail());
            response.setJwt(jwt);
            response.setHasProfile(user.isHasProfile());
            return response;
        } catch (Exception exception) {
            throw new UserServiceException(ErrorMessages.AUTHENTICATION_FAILED.getErrorMessage());
        }
    }

    public void createUser(UserModelRequest userModelRequest) {

        User user = createUserEntity(userModelRequest);
        user.setId(sequenceGenerator.getSequenceNumber(User.SEQUENCE_NAME));
        try {
            repository.save(user);
        } catch (Exception exception) {
            throw new UserServiceException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage() + exception.getMessage());
        }

    }

    private User createUserEntity(UserModelRequest userModelRequest) {
        User user = MAPPER.map(userModelRequest, User.class);
        user.setEncryptedPassword(passwordEncoder.encode(userModelRequest.getPassword()));
        user.setPublicId(util.generatePublicId(10));
        return user;
    }


    public UserRest getUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        User entity = repository.findByEmail(username);
        if (entity == null) {
            throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
        }
        return MAPPER.map(entity, UserRest.class);
    }

    public void saveImage(String url) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String emailAddress = authentication.getName();

        RestTemplate restTemplate = new RestTemplate();
        byte[] imageBytes = restTemplate.getForObject(url, byte[].class);

        User user = repository.findByEmail(emailAddress);
        user.setProfilePhoto(imageBytes);
        user.setHasProfile(true);
        repository.save(user);

    }

    public String getCurrentProfilePhoto() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String emailAddress = authentication.getName();
        User user = repository.findByEmail(emailAddress);
        return new String(user.getProfilePhoto());
    }


}
