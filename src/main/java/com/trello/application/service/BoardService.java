package com.trello.application.service;

import com.trello.application.entity.Board;
import com.trello.application.entity.Card;
import com.trello.application.entity.CardsList;
import com.trello.application.entity.User;
import com.trello.application.model.request.BoardModelRequest;
import com.trello.application.model.request.CardInfo;
import com.trello.application.model.request.ListDetails;
import com.trello.application.repository.BoardRepository;
import com.trello.application.repository.CardsListRepository;
import com.trello.application.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class BoardService {
    private final BoardRepository boardRepository;
    private final UserRepository userRepository;
    private final CardsListRepository cardsListRepository;
    private final SequenceGenerator sequenceGenerator;
    private final ModelMapper modelMapper = new ModelMapper();

    public Board creatBoard(BoardModelRequest boardModelRequest) {
        Board board = modelMapper.map(boardModelRequest, Board.class);
        board.setId(sequenceGenerator.getSequenceNumber(Board.SEQUENCE_NAME));

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String emailAddress = authentication.getName();
        User user = userRepository.findByEmail(emailAddress);
        board.setUserEmailAddress(user.getEmail());
        return boardRepository.save(board);
    }

    public Board editBoard(long boardId, BoardModelRequest boardModelRequest) {
        Optional<Board> optionalBoard = boardRepository.findById(boardId);
        Board board = optionalBoard.orElseThrow();
        board.setName(boardModelRequest.getName());
        return boardRepository.save(board);
    }

    public void deleteBoard(long boardId) {
        boardRepository.deleteById(boardId);
        cardsListRepository.deleteAllByBoardId(boardId);
    }

    public List<Board> getAllBoards() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String emailAddress = authentication.getName();
        User user = userRepository.findByEmail(emailAddress);
        return boardRepository.getAllByUserEmailAddress(user.getEmail());
    }


    public CardsList addListToBoard(long boardId, ListDetails listDetails) {
        getProperBoard(boardId);
        CardsList cardsCollection = new CardsList();
        cardsCollection.setName(listDetails.getName());
        cardsCollection.setBoardId(boardId);
        cardsCollection.setId(sequenceGenerator.getSequenceNumber(CardsList.SEQUENCE_NAME));
        return cardsListRepository.save(cardsCollection);
    }

    public void editCardsList(long boardsId, long listId, ListDetails listDetails) {
        Optional<CardsList> optionalCardsList = cardsListRepository.findById(listId);
        CardsList cardsList = optionalCardsList.orElseThrow(NoSuchElementException::new);
        cardsList.setName(listDetails.getName());
        cardsListRepository.save(cardsList);
    }

    public void deleteCardsList(long boardsId, long listId) {
        cardsListRepository.deleteById(listId);
    }

    public List<CardsList> getCardsListBoard(long boardId) {
        return cardsListRepository.getAllByBoardId(boardId);
    }

    public CardsList addCardToBoardList(long boardId, long cardListId, CardInfo cardInfo) {
        getProperBoard(boardId);
        Optional<CardsList> optionalCardsList = cardsListRepository.findById(cardListId);
        CardsList cardsList = optionalCardsList.orElseThrow();
        Card card = modelMapper.map(cardInfo, Card.class);
        card.setId(sequenceGenerator.getSequenceNumber(Card.SEQUENCE_NAME));
        cardsList.getCards().add(card);
        return cardsListRepository.save(cardsList);
    }

    public CardsList editCardFromBoardList(long boardId, long cardListId, long cardId, CardInfo cardInfo) {
        getProperBoard(boardId);
        Optional<CardsList> optionalCardsList = cardsListRepository.findById(cardListId);
        CardsList cardsList = optionalCardsList.orElseThrow();
        for (Card card : cardsList.getCards()) {
            if (card.getId() == cardId) {
                card.setDescription(cardInfo.getDescription());
                card.setTitle(cardInfo.getTitle());
                break;
            }
        }
        return cardsListRepository.save(cardsList);
    }

    public void deleteCardFromBoardList(long boardId, long cardListId, long cardId) {
        getProperBoard(boardId);
        Optional<CardsList> optionalCardsList = cardsListRepository.findById(cardListId);
        CardsList cardsList = optionalCardsList.orElseThrow();
        Card searchedCard = null;
        for (Card card : cardsList.getCards()) {
            if (card.getId() == cardId) {
                searchedCard = card;
                break;
            }
        }

        cardsList.getCards().remove(searchedCard);
        cardsListRepository.save(cardsList);
    }

    private void getProperBoard(long boardId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String emailAddress = authentication.getName();
        User user = userRepository.findByEmail(emailAddress);

        Optional<Board> boardOptional = boardRepository.findById(boardId);
        Board board = boardOptional.orElseThrow(NoSuchElementException::new);
        if (!board.getUserEmailAddress().equals(user.getEmail())) {
            throw new NoSuchElementException();
        }
    }

    public Board getBoard(long boardId) {
        Optional<Board> optionalBoard = boardRepository.findById(boardId);
        return optionalBoard.orElseThrow();
    }
}
