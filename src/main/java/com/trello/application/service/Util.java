package com.trello.application.service;

import com.trello.application.exception.UserServiceException;
import com.trello.application.model.response.ErrorMessages;
import com.trello.application.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.Random;


@RequiredArgsConstructor
@Service
public class Util {
    private static final Random RANDOM = new SecureRandom();
    private static final String ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private final Path photoDirectory = Paths.get(System.getProperty("user.dir") + "/photos/");

    public final UserRepository userRepository;


    public String generatePublicId(int length) {
        return generateRandomString(length);
    }

    private String generateRandomString(int length) {
        StringBuilder returnValue = new StringBuilder(length);

        for (int i = 0; i < length; i++) {
            returnValue.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
        }

        return returnValue.toString();
    }

    public Path getPathForLocalStoredFile(byte[] bytes, String emailAddress) {

        Path path = Paths.get(photoDirectory.toString(), emailAddress + ".svg");
        try {
            Files.write(path, bytes);
        } catch (IOException e) {
            throw new UserServiceException(ErrorMessages.COULD_NOT_SAVE_IMAGE.getErrorMessage() + e.getMessage());
        }
        return path;
    }

}
