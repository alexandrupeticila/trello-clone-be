package com.trello.application.service;

import com.trello.application.entity.DbSequence;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;

@Service
@RequiredArgsConstructor
public class SequenceGenerator {

    private final MongoOperations mongoOperations;

    public int getSequenceNumber(String sequenceName) {
        // get sequence number
        Query query = new Query(Criteria.where("id").is(sequenceName));
        // update sequence number
        Update update = new Update().inc("seq", 1);
        // modify sequence in document
        DbSequence counter = mongoOperations.findAndModify(
                query,
                update,
                options().returnNew(true).upsert(true),
                DbSequence.class
        );

        if (counter != null) {
            return counter.getSeq();
        }
        return 1;
    }
}
