package com.trello.application.repository;

import com.trello.application.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, Long> {

    boolean existsByEmail(String email);

    void deleteByPublicId(String id);

    User findByPublicId(String userPublicId);

    User findByEmail(String email);
}
