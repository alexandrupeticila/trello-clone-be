package com.trello.application.repository;

import com.trello.application.entity.CardsList;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardsListRepository extends MongoRepository<CardsList, Long> {
    List<CardsList> getAllByBoardId(long boardId);
    void deleteAllByBoardId(long boardId);
}
