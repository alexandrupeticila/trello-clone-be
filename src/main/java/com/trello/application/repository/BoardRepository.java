package com.trello.application.repository;

import com.trello.application.entity.Board;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BoardRepository extends MongoRepository<Board, Long> {
    List<Board> getAllByUserEmailAddress(String userEmailAddress);
}
