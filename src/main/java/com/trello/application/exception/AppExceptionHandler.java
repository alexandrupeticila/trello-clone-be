package com.trello.application.exception;


import com.trello.application.model.response.ErrorMessageStructure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

/**
 * Handle Exceptions
 */
@ControllerAdvice
public class AppExceptionHandler {
    private final Logger logger = LoggerFactory.getLogger(AppExceptionHandler.class);

    @ExceptionHandler(value = {UserServiceException.class})
    public ResponseEntity<Object> handleUserServiceException(UserServiceException exception, WebRequest request) {
        ErrorMessageStructure errorMessage = new ErrorMessageStructure(new Date(), exception.getMessage());
        logger.error(errorMessage.getMessage() + "" + new Date(System.currentTimeMillis()).toString());
        return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    public ResponseEntity<Object> handleMethodArgumentNotValidException(MethodArgumentNotValidException exception, WebRequest request) {
        FieldError error = (FieldError) exception.getBindingResult().getAllErrors().get(0);
        String exceptionMessage = error.getDefaultMessage();
        ErrorMessageStructure errorMessage = new ErrorMessageStructure(new Date(), exceptionMessage);
        logger.error(errorMessage.getMessage() + "" + new Date(System.currentTimeMillis()).toString());
        return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<Object> handleOtherException(Exception exception, WebRequest request) {
        ErrorMessageStructure errorMessage = new ErrorMessageStructure(new Date(), exception.getMessage());
        logger.error(errorMessage.getMessage() + "" + new Date(System.currentTimeMillis()).toString());
        return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
