package com.trello.application.security;

import com.trello.application.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@RequiredArgsConstructor
@Service
public class MyUserDetails implements UserDetailsService {
    private final UserRepository repository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        String encryptedPassword;

        com.trello.application.entity.User user = repository.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException("User with the email: '" + email + "' not found");
        }
        encryptedPassword = user.getEncryptedPassword();

        return User//
                .withUsername(email)//
                .password(encryptedPassword)//
                .authorities(Collections.singletonList(new SimpleGrantedAuthority(user.getPublicId())))//
                .accountExpired(false)//
                .accountLocked(false)//
                .credentialsExpired(false)//
                .disabled(false)//
                .build();
    }
}