package com.trello.application.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Document(collection = "users")
public class User implements Serializable {
    private static final long serialVersionUID = 5460759139762648332L;

    @Transient
    public static final String SEQUENCE_NAME = "user_sequence";

    @Id
    private long id;

    private String publicId;

    private String email;

    @NotNull
    private String encryptedPassword;

    private String lastName;

    private String firstName;

    private byte[] profilePhoto;
    private boolean hasProfile = false;
}
