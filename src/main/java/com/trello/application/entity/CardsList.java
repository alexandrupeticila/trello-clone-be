package com.trello.application.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Data
@Document( collection = "cards_set")
public class CardsList{
    @Transient
    public static final String SEQUENCE_NAME = "cards_set_sequence";
    @Id
    private long id;
    private long boardId;
    private String name;
    private String color;
    private List<Card> cards = new ArrayList<>();
}
