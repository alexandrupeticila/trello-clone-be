package com.trello.application.entity;

import lombok.Data;

@Data
public class Card {
    public static final String SEQUENCE_NAME = "card_sequence";
    private long id;
    private String title;
    private String description;
}
