package com.trello.application.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "boards")
public class Board {

    @Transient
    public static final String SEQUENCE_NAME = "board_sequence";

    @Id
    private long id;
    private String name;
    private String color;
    private String userEmailAddress;
}
