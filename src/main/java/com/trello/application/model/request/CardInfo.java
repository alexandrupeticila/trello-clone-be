package com.trello.application.model.request;

import lombok.Data;

@Data
public class CardInfo {
    private String title;
    private String description;
}
