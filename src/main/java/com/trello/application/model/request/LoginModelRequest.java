package com.trello.application.model.request;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Data
public class LoginModelRequest {

    @Email
    private String email;
    private String password;
}
