package com.trello.application.model.request;

import lombok.Data;

@Data
public class ImageUrlModelRequest {
    private String link;
}
