package com.trello.application.model.request;

import com.trello.application.model.PatternTypes;
import com.trello.application.model.response.ErrorMessages;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;


@Data
@Validated
public class UserModelRequest {
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    @Email
    private String email;

    @Pattern(regexp = PatternTypes.PASSWORD_PATTERN,
            message = ErrorMessages.PASSWORD_DOES_NOT_RESPECT_CONSTRAINTS)
    private String password;
}
