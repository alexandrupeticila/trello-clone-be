package com.trello.application.model.request;

import lombok.Data;

@Data
public class BoardModelRequest {
    private String name;
    private String color;
}
