package com.trello.application.model.request;

import lombok.Data;

@Data
public class ListDetails {
    private String name;
    private String color;
}
