package com.trello.application.model.response;

import lombok.Data;

@Data
public class LoginModelResponse {
    private String jwt;
    private boolean hasProfile;
}
