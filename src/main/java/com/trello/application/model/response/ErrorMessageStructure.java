package com.trello.application.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

/**
 * Structure for exception message
 */
@Data
@AllArgsConstructor
public class ErrorMessageStructure {
    private Date timeStamp;
    private String message;
}

