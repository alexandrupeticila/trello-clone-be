package com.trello.application.model.response;

import lombok.Data;

import java.sql.Time;
import java.util.Date;

/**
 * Used for sending information to the client
 */
@Data
public class UserRest {
    private String publicId;
    private String firstName;
    private String lastName;
    private String email;
    private String photo;
}
