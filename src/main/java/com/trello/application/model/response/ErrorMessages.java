package com.trello.application.model.response;

public enum ErrorMessages {

    MISSING_REQUIRED_FIELD("Missing required field. Please check documentation for required fields"),
    RECORD_ALREADY_EXISTS("Record already exists"),
    INTERNAL_SERVER_ERROR("Internal server error"),
    NO_RECORD_FOUND("Record with provided id is not found"),
    AUTHENTICATION_FAILED("Authentication failed"),
    COULD_NOT_UPDATE_RECORD("Could not update record"),
    COULD_NOT_DELETE_RECORD("Could not delete record"),
    COULD_NOT_SAVE_RECORD("Could not save record"),
    NO_USER_WITH_THIS_EMAIL("Nu exista nici un utilizator cu acest email"),
    COULD_NOT_SAVE_IMAGE("Could not save image"),
    DOES_NOT_HAVE_IMAGE("Doesn't have image");
    public static final String PASSWORD_DOES_NOT_RESPECT_CONSTRAINTS = "The password must have minimum 8 characters, one uppercase, one lowercase, one number and one special key";

    private String errorMessage;

    ErrorMessages(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
