package com.trello.application.controller;


import com.trello.application.model.request.ImageUrlModelRequest;
import com.trello.application.model.request.LoginModelRequest;
import com.trello.application.model.request.UserModelRequest;
import com.trello.application.model.response.LoginModelResponse;
import com.trello.application.model.response.UserRest;
import com.trello.application.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequiredArgsConstructor
@RequestMapping("/user")
@RestController
public class UserController {
    private final UserService service;

    @PostMapping("/login")
    public LoginModelResponse signIn(@Valid @RequestBody LoginModelRequest loginInfo) {
        return service.authenticate(loginInfo);
    }

    @PostMapping("/add")
    public ResponseEntity<String> createUser(@Valid @RequestBody UserModelRequest userModelRequest) {
        service.createUser(userModelRequest);
        return new ResponseEntity<>("Created", HttpStatus.CREATED);
    }


    @GetMapping(value = "/profile")
    public UserRest getUser() {
        return service.getUser();
    }


    @PostMapping("/saveImage")
    public ResponseEntity<String> test(@RequestBody ImageUrlModelRequest request) {
        service.saveImage(request.getLink());
        return ResponseEntity.ok("success");
    }

    @GetMapping(value = "/profile/photo")
    public String getVolunteerProfilePhoto() {
        return service.getCurrentProfilePhoto();
    }

}
