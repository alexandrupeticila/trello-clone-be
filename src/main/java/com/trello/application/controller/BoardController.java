package com.trello.application.controller;

import com.trello.application.entity.Board;
import com.trello.application.entity.CardsList;
import com.trello.application.model.request.BoardModelRequest;
import com.trello.application.model.request.CardInfo;
import com.trello.application.model.request.ListDetails;
import com.trello.application.service.BoardService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/boards")
@RequiredArgsConstructor
@RestController
public class BoardController {
    private final BoardService boardService;

    @PostMapping("/create")
    public ResponseEntity<Board> createBoard(@RequestBody BoardModelRequest boardModelRequest) {
        Board board = boardService.creatBoard(boardModelRequest);
        return new ResponseEntity<>(board, HttpStatus.CREATED);
    }

    @PutMapping("/edit/{boardId}")
    public ResponseEntity<Board> editBoard(@PathVariable long boardId, @RequestBody BoardModelRequest boardModelRequest) {
        Board board = boardService.editBoard(boardId, boardModelRequest);
        return new ResponseEntity<>(board, HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{boardId}")
    public ResponseEntity<String> deleteBoard(@PathVariable long boardId) {
        boardService.deleteBoard(boardId);
        return new ResponseEntity<>("success", HttpStatus.OK);
    }

    @GetMapping("/getAll")
    public List<Board> getAllBoards() {
        return boardService.getAllBoards();
    }


    @GetMapping("/getBoard/{boardId}")
    public Board getBoard(@PathVariable long boardId){
        return boardService.getBoard(boardId);
    }

    @GetMapping("/getCardsList/{boardId}")
    public List<CardsList> getCardsListByBoard(@PathVariable long boardId) {
        return boardService.getCardsListBoard(boardId);
    }

    @PostMapping("/createList/{boardId}")
    public ResponseEntity<CardsList> addListToBoard(@PathVariable("boardId") long boardId,
                                                    @RequestBody ListDetails listDetails) {
        CardsList cardsList = boardService.addListToBoard(boardId, listDetails);
        return new ResponseEntity<>(cardsList, HttpStatus.OK);
    }

    @PutMapping("/editList/{boardsId}/{listId}")
    public ResponseEntity<String> editCardsList(@PathVariable long boardsId, @PathVariable long listId,
                                                @RequestBody ListDetails listDetails) {
        boardService.editCardsList(boardsId, listId, listDetails);
        return new ResponseEntity<>("success", HttpStatus.OK);
    }

    @DeleteMapping("/deleteList/{boardsId}/{listId}")
    public ResponseEntity<String> deleteCardsList(@PathVariable long boardsId, @PathVariable long listId) {
        boardService.deleteCardsList(boardsId, listId);
        return new ResponseEntity<>("success", HttpStatus.OK);
    }

    @PostMapping("/createCard/{boardId}/{cardListId}")
    public ResponseEntity<CardsList> addCardToList(@PathVariable("boardId") long boardId,
                                                   @PathVariable("cardListId") long cardListId,
                                                   @RequestBody CardInfo cardInfo) {
        CardsList cardsList = boardService.addCardToBoardList(boardId, cardListId, cardInfo);
        return new ResponseEntity<>(cardsList, HttpStatus.CREATED);
    }

    @PutMapping("/editCard/{boardId}/{cardListId}/{cardId}")
    public ResponseEntity<CardsList> editCardFromList(@PathVariable("boardId") long boardId,
                                                      @PathVariable("cardListId") long cardListId,
                                                      @PathVariable("cardId") long cardId,
                                                      @RequestBody CardInfo cardInfo) {
        CardsList cardsList = boardService.editCardFromBoardList(boardId, cardListId, cardId, cardInfo);
        return new ResponseEntity<>(cardsList, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{boardId}/{cardListId}/{cardId}")
    public ResponseEntity<String> deleteCardFromList(@PathVariable("boardId") long boardId,
                                                        @PathVariable("cardListId") long cardListId,
                                                        @PathVariable("cardId") long cardId) {
        boardService.deleteCardFromBoardList(boardId, cardListId, cardId);
        return new ResponseEntity<>("success", HttpStatus.OK);
    }


}
