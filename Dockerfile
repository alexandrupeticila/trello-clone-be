FROM alpine:edge
MAINTAINER alexandru.peticila@yahoo.com
RUN apk add --no-cache openjdk11
COPY ./target/restApi-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app.jar"]